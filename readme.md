Apache Camel pollEnrich example
===============================


Run
-
* mvn spring-boot:run
* HTTP GET op [http://localhost:8090/trigger](http://localhost:8090/trigger)
* kopieer een klein bestand naar target/pollingInput, bijvoorbeeld input.txt

Wat doet het
-
* na een GET op /trigger
* trigger de route met een template.requestBody() (=InOut pattern), in tegenstelling tot sendBody (=InOnly pattern)
* poll voor een bestand in target/pollingInput
* na poll, split met een custom splitter en AggregationStrategy
* de splitter maakt uit 1 bericht altijd 10 berichten met als body "split: element: x"
* de aggregator houdt een teller bij in Exchange header "counter"
* na aggregatie wordt het aantal in de body gezet
* die body wordt als resultaat teruggegeven voor template.requestBody()
* het resultaat wordt teruggegeven in de http response



