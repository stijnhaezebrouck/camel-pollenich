package pollenrich;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

public class AggregationCounter implements AggregationStrategy {
    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        if (oldExchange == null) {
            newExchange.getIn().setHeader("counter", 1);
        } else {
            int counter = oldExchange.getIn().getHeader("counter",Integer.class);
            counter++;
            newExchange.getIn().setHeader("counter", counter);
        }
        return newExchange;
    }
}
