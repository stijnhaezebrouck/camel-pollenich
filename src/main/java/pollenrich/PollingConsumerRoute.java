package pollenrich;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import static org.apache.camel.LoggingLevel.INFO;

@Component
public class PollingConsumerRoute extends RouteBuilder {

    public static final String START = "direct:start";


    @Override
    public void configure() {

        from(START)
                .routeId("pollingConsumerRoute")
                .log(INFO, getClass().getName(),">start van de route ${body}<")
                .pollEnrich("file:target/pollingInput")
                .log(INFO, getClass().getName(), ">after polling for file: ${body}<")
                .setBody(constant("dit zit er voor het splitsen in het lichaam"))
                .split().method(Splitter.class).aggregationStrategy(new AggregationCounter())
                    .log(INFO, getClass().getName(), ">split: ${body}<")
                .end()
                .log(INFO, getClass().getName(), ">>afterAggregation: count=${in.headers.counter}")
                .setBody(constant("aggregationCounter=").append(header("counter")));
    }
}
