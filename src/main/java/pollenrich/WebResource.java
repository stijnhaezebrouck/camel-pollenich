package pollenrich;


import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static pollenrich.PollingConsumerRoute.START;

@RestController
public class WebResource {

    private Logger log = LoggerFactory.getLogger(getClass());


    @EndpointInject(uri=START)
    private ProducerTemplate producer;

    @RequestMapping("/trigger")
    public String trigger() {
        String teruggeefwaarde = producer.requestBody((Object)"route trigger", String.class);
        log.info("route uitgevoerd, dit zat er op het einde in de body: " + teruggeefwaarde);
        return teruggeefwaarde;
    }
}
