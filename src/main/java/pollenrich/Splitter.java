package pollenrich;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component("splitter")
public class Splitter {

    private Logger log = LoggerFactory.getLogger(getClass());

    public List<String> split(String body) {

            return list(10);

    }

    private List<String> list(int aantal) {
        return
                IntStream.rangeClosed(1, aantal)
                        .mapToObj(i->"element: "+i)
                        .collect(Collectors.toList());
    }

}
