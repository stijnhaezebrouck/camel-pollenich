package pollenrich;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class Run {

    @Value("${server.port}")
    private int port;

    public static void main(String... args) {
        SpringApplication.run(Run.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void printReady() {
        System.out.println("Gestart, GET op http://localhost:" + port + "/trigger en kopieer input.txt naar target/pollingInput");
    }

}
